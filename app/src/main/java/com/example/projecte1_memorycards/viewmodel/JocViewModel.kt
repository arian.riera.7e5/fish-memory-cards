package com.example.projecte1_memorycards.viewmodel

import androidx.lifecycle.ViewModel
import com.example.projecte1_memorycards.R
import com.example.projecte1_memorycards.model.Cards
import java.util.Collections.shuffle

class JocViewModel: ViewModel() {

    // Al final no he set capaç d'implmentar el viewmodel en aquest projecte

    val images: Array<Int> = arrayOf(R.drawable.pescado1, R.drawable.pescado2, R.drawable.pescado3, R.drawable.pescado4, R.drawable.pescado5)
    val myCards = mutableListOf<Cards>()
    var movements = 0

    init {
        setDataModel()
    }

    private fun setDataModel() {
        images.shuffle()
        val randomNumbers = listOf(1,2,3,1,2,3)
        shuffle(randomNumbers)
        for (i in randomNumbers) {
            myCards.add(Cards(i, images[i]))
        }
    }

    fun changeButtonImage(numImage: Int) : Int {
        movements++
        val fishImages: Array<Int> = arrayOf(0, R.drawable.pescado1, R.drawable.pescado2, R.drawable.pescado3, R.drawable.pescado4, R.drawable.pescado5)
        return fishImages[numImage]
    }

}