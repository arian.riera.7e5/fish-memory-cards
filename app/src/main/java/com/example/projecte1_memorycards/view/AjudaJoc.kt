package com.example.projecte1_memorycards.view

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.example.projecte1_memorycards.databinding.ActivityAjudaJocBinding

class AjudaJoc : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityAjudaJocBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val goMenu: Button = binding.returnToMenu
        val nightMode: Button = binding.mode
        val how2Play: Button = binding.howToPlay
        val how2PlayText: TextView = binding.howToPlayText
        val contact: Button = binding.contact
        val bugs: Button = binding.bugs


        goMenu.setOnClickListener {
            val intent = Intent(this, MenuJoc::class.java)
            startActivity(intent)
        }

        nightMode.setOnClickListener {
            when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                Configuration.UI_MODE_NIGHT_YES ->
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                Configuration.UI_MODE_NIGHT_NO ->
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
        }

        how2Play.setOnClickListener {
            goMenu.visibility = View.INVISIBLE
            nightMode.visibility = View.INVISIBLE
            how2Play.visibility = View.INVISIBLE
            how2PlayText.visibility = View.VISIBLE
            contact.visibility = View.INVISIBLE
            bugs.visibility = View.INVISIBLE
        }

        how2PlayText.setOnClickListener {
            goMenu.visibility = View.VISIBLE
            nightMode.visibility = View.VISIBLE
            how2Play.visibility = View.VISIBLE
            how2PlayText.visibility = View.INVISIBLE
            contact.visibility = View.VISIBLE
            bugs.visibility = View.VISIBLE
        }

        contact.setOnClickListener {
            Toast.makeText(this, "Pots contactar amb nosaltres a traves del nostre correu: FiMeGa@gmail.com", Toast.LENGTH_LONG).show()
        }

        bugs.setOnClickListener {
            Toast.makeText(this, "Per a notificar un bug, manda una imatge i una descripció al correu bugsFMG@gmail.com", Toast.LENGTH_LONG).show()
        }

    }
}