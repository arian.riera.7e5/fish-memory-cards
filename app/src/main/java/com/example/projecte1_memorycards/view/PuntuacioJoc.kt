package com.example.projecte1_memorycards.view

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.projecte1_memorycards.R
import com.example.projecte1_memorycards.databinding.ActivityPuntuacioJocBinding

class PuntuacioJoc : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityPuntuacioJocBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val score = bundle?.getInt("score")
        val numCards = bundle?.getInt("numCards")
        val scoreText: TextView = binding.score
        val text4Score: TextView = binding.phrase

        scoreText.text = score.toString()

        when {
            score!! >= 750000 -> {text4Score.text = resources.getString(R.string.frase_segons_puntuacio1)}
            score in 500000..749999 -> {text4Score.text = resources.getString(R.string.frase_segons_puntuacio2)}
            score in 250000..499999 -> {text4Score.text = resources.getString(R.string.frase_segons_puntuacio3)}
            score < 250000 -> {text4Score.text = resources.getString(R.string.frase_segons_puntuacio4)}
        }

        val goMenu: Button = binding.backToMenu
        val goGame: Button = binding.retry
        var intent: Intent

        goMenu.setOnClickListener {
            intent = Intent(this, MenuJoc::class.java)
            startActivity(intent)
        }

        goGame.setOnClickListener {
            intent = Intent(this, Joc::class.java)
            intent.putExtra("numCards", numCards)
            startActivity(intent)
        }

    }
}