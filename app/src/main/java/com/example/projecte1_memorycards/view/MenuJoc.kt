package com.example.projecte1_memorycards.view

import android.content.Intent
import android.os.Bundle
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.example.projecte1_memorycards.databinding.ActivityMenuJocBinding
import android.widget.ArrayAdapter
import android.widget.Button
import com.example.projecte1_memorycards.R

class MenuJoc : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Projecte1_MemoryCards)
        super.onCreate(savedInstanceState)
        val binding = ActivityMenuJocBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val difficultySelector: Spinner = binding.difficultySpinner
        val playButton: Button = binding.playButton
        val helpButton: Button = binding.helpButton

        val arraySpinner = arrayOf("Fácil", "Normal", "Difícil")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, arraySpinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        difficultySelector.adapter = adapter

        var intent: Intent

        playButton.setOnClickListener {
            val difficultyChoosen = difficultySelector.selectedItem.toString()
            if (difficultyChoosen == "Fácil") {
                intent = Intent(this, Joc::class.java)
                intent.putExtra("numCards", 6)
                startActivity(intent)
            }
            if (difficultyChoosen == "Normal") {
                intent = Intent(this, Joc::class.java)
                intent.putExtra("numCards", 8)
                startActivity(intent)
            }
            if (difficultyChoosen == "Difícil") {
                intent = Intent(this, Joc::class.java)
                intent.putExtra("numCards", 10)
                startActivity(intent)
            }
        }

        helpButton.setOnClickListener {
            intent = Intent(this, AjudaJoc::class.java)
            intent.extras
            startActivity(intent)
        }

    }
}