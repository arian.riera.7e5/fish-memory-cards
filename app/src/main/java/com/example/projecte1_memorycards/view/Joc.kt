package com.example.projecte1_memorycards.view

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.util.Collections.shuffle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.Guideline
import androidx.lifecycle.ViewModelProvider
import com.example.projecte1_memorycards.R
import com.example.projecte1_memorycards.databinding.ActivityJocBinding
import com.example.projecte1_memorycards.viewmodel.JocViewModel


class Joc : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityJocBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val handler = Handler(Looper.getMainLooper())

        // ----------------------------  CHRONOMETER -------------------------------

        val chronometer: Chronometer = binding.timer
        chronometer.start()

        // ----------------------------  PAUSE BUTTON -------------------------------

        val pause: Button = binding.pause
        var freeze = false
        var pauseOffset: Long = 0

        pause.setOnClickListener {
            if (!freeze) {
                chronometer.stop()
                pauseOffset = SystemClock.elapsedRealtime()-chronometer.base
                freeze = true
                pause.setBackgroundResource(R.drawable.play)
            } else {
                chronometer.base = SystemClock.elapsedRealtime()-pauseOffset
                chronometer.start()
                freeze = false
                pause.setBackgroundResource(R.drawable.pause)
            }
        }

        // ---------------------------- CREATE GAME ----------------------------

        val bundle: Bundle? = intent.extras
        val numCards = bundle?.getInt("numCards")

        val card1: ImageButton = binding.card1
        val card2: ImageButton = binding.card2
        val card3: ImageButton = binding.card3
        val card4: ImageButton = binding.card4
        val card5: ImageButton = binding.card5
        val card6: ImageButton = binding.card6
        var cardsArray = arrayOf(card1, card2, card3, card4, card5, card6)
        val randomNumbersPrimitive = listOf(1,2,3,1,2,3)
        lateinit var randomNumbers: List<Int>

        var difficulty = 0

        when (numCards) {

            6 -> {
                randomNumbers = randomNumbersPrimitive
            }

            8 -> {

                difficulty = 100000

                for (i in cardsArray.indices) {
                    setImageButton(cardsArray[i], 100.toPx())
                }

                val constraintLayout = binding.constraintLayout
                val guideline: Guideline = binding.guideline
                val guideline1: Guideline = binding.guideline1
                val guideline7: Guideline = binding.guideline7
                val guideline4 = binding.guideline4
                setGuideline(guideline4, 0.45f)
                val guideline5 = binding.guideline5
                setGuideline(guideline5, 0.65f)

                val card7 = ImageButton(this)
                card7.id = View.generateViewId()
                card7.adjustViewBounds = true
                card7.scaleType = ImageView.ScaleType.FIT_CENTER
                card7.setPadding(20.toPx(), 20.toPx(), 20.toPx(), 20.toPx())
                constraintLayout.addView(card7)
                setImageButton(card7, 100.toPx())
                card7.setBackgroundResource(R.drawable.border)
                cardsArray = appendImageView(cardsArray, card7)

                val set = ConstraintSet()
                set.clone(constraintLayout)
                set.connect(card7.id, ConstraintSet.TOP, guideline7.id, ConstraintSet.TOP)
                set.connect(card7.id, ConstraintSet.BOTTOM, guideline7.id, ConstraintSet.BOTTOM)
                set.connect(card7.id, ConstraintSet.START, guideline1.id, ConstraintSet.START)
                set.connect(card7.id, ConstraintSet.END, guideline1.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                val card8 = ImageButton(this)
                card8.id = View.generateViewId()
                card8.adjustViewBounds = true
                card8.scaleType = ImageView.ScaleType.FIT_CENTER
                card8.setPadding(20.toPx(), 20.toPx(), 20.toPx(), 20.toPx())
                constraintLayout.addView(card8)
                setImageButton(card8, 100.toPx())
                card8.setBackgroundResource(R.drawable.border)
                cardsArray = appendImageView(cardsArray, card8)

                set.clone(constraintLayout)
                set.connect(card8.id, ConstraintSet.TOP, guideline7.id, ConstraintSet.TOP)
                set.connect(card8.id, ConstraintSet.BOTTOM, guideline7.id, ConstraintSet.BOTTOM)
                set.connect(card8.id, ConstraintSet.START, guideline.id, ConstraintSet.START)
                set.connect(card8.id, ConstraintSet.END, guideline.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                randomNumbers = randomNumbersPrimitive + 4 + 4

            }

            10 -> {

                difficulty = 200000

                for (i in cardsArray.indices) {
                    setImageButton(cardsArray[i], 100.toPx())
                }

                val constraintLayout = binding.constraintLayout
                val guideline: Guideline = binding.guideline
                setGuideline(guideline, 0.8f)
                val guideline1: Guideline = binding.guideline1
                setGuideline(guideline1, 0.2f)
                val guideline3: Guideline = binding.guideline3
                val guideline4 = binding.guideline4
                setGuideline(guideline4, 0.45f)
                val guideline5 = binding.guideline5
                setGuideline(guideline5, 0.65f)
                val guideline6: Guideline = binding.guideline6
                val guideline7: Guideline = binding.guideline7

                val set = ConstraintSet()

                // Card 2
                set.clone(constraintLayout)
                set.connect(card2.id, ConstraintSet.TOP, guideline3.id, ConstraintSet.TOP)
                set.connect(card2.id, ConstraintSet.BOTTOM, guideline3.id, ConstraintSet.BOTTOM)
                set.connect(card2.id, ConstraintSet.START, guideline6.id, ConstraintSet.START)
                set.connect(card2.id, ConstraintSet.END, guideline6.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 3
                set.clone(constraintLayout)
                set.connect(card3.id, ConstraintSet.TOP, guideline3.id, ConstraintSet.TOP)
                set.connect(card3.id, ConstraintSet.BOTTOM, guideline3.id, ConstraintSet.BOTTOM)
                set.connect(card3.id, ConstraintSet.START, guideline.id, ConstraintSet.START)
                set.connect(card3.id, ConstraintSet.END, guideline.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 4
                set.clone(constraintLayout)
                set.connect(card4.id, ConstraintSet.TOP, guideline4.id, ConstraintSet.TOP)
                set.connect(card4.id, ConstraintSet.BOTTOM, guideline4.id, ConstraintSet.BOTTOM)
                set.connect(card4.id, ConstraintSet.START, guideline1.id, ConstraintSet.START)
                set.connect(card4.id, ConstraintSet.END, guideline1.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 5
                set.clone(constraintLayout)
                set.connect(card5.id, ConstraintSet.TOP, guideline4.id, ConstraintSet.TOP)
                set.connect(card5.id, ConstraintSet.BOTTOM, guideline4.id, ConstraintSet.BOTTOM)
                set.connect(card5.id, ConstraintSet.START, guideline6.id, ConstraintSet.START)
                set.connect(card5.id, ConstraintSet.END, guideline6.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 6
                set.clone(constraintLayout)
                set.connect(card6.id, ConstraintSet.TOP, guideline4.id, ConstraintSet.TOP)
                set.connect(card6.id, ConstraintSet.BOTTOM, guideline4.id, ConstraintSet.BOTTOM)
                set.connect(card6.id, ConstraintSet.START, guideline.id, ConstraintSet.START)
                set.connect(card6.id, ConstraintSet.END, guideline.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 7
                val card7 = ImageButton(this)
                card7.id = View.generateViewId()
                card7.adjustViewBounds = true
                card7.scaleType = ImageView.ScaleType.FIT_CENTER
                card7.setPadding(20.toPx(), 20.toPx(), 20.toPx(), 20.toPx())
                constraintLayout.addView(card7)
                setImageButton(card7, 100.toPx())
                card7.setBackgroundResource(R.drawable.border)
                cardsArray = appendImageView(cardsArray, card7)

                set.clone(constraintLayout)
                set.connect(card7.id, ConstraintSet.TOP, guideline5.id, ConstraintSet.TOP)
                set.connect(card7.id, ConstraintSet.BOTTOM, guideline5.id, ConstraintSet.BOTTOM)
                set.connect(card7.id, ConstraintSet.START, guideline1.id, ConstraintSet.START)
                set.connect(card7.id, ConstraintSet.END, guideline1.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 8
                val card8 = ImageButton(this)
                card8.id = View.generateViewId()
                card8.adjustViewBounds = true
                card8.scaleType = ImageView.ScaleType.FIT_CENTER
                card8.setPadding(20.toPx(), 20.toPx(), 20.toPx(), 20.toPx())
                constraintLayout.addView(card8)
                setImageButton(card8, 100.toPx())
                card8.setBackgroundResource(R.drawable.border)
                cardsArray = appendImageView(cardsArray, card8)

                set.clone(constraintLayout)
                set.connect(card8.id, ConstraintSet.TOP, guideline5.id, ConstraintSet.TOP)
                set.connect(card8.id, ConstraintSet.BOTTOM, guideline5.id, ConstraintSet.BOTTOM)
                set.connect(card8.id, ConstraintSet.START, guideline6.id, ConstraintSet.START)
                set.connect(card8.id, ConstraintSet.END, guideline6.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 9
                val card9 = ImageButton(this)
                card9.id = View.generateViewId()
                card9.adjustViewBounds = true
                card9.scaleType = ImageView.ScaleType.FIT_CENTER
                card9.setPadding(20.toPx(), 20.toPx(), 20.toPx(), 20.toPx())
                constraintLayout.addView(card9)
                setImageButton(card9, 100.toPx())
                card9.setBackgroundResource(R.drawable.border)
                cardsArray = appendImageView(cardsArray, card9)

                set.clone(constraintLayout)
                set.connect(card9.id, ConstraintSet.TOP, guideline5.id, ConstraintSet.TOP)
                set.connect(card9.id, ConstraintSet.BOTTOM, guideline5.id, ConstraintSet.BOTTOM)
                set.connect(card9.id, ConstraintSet.START, guideline.id, ConstraintSet.START)
                set.connect(card9.id, ConstraintSet.END, guideline.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                // Card 10
                val card10 = ImageButton(this)
                card10.id = View.generateViewId()
                card10.adjustViewBounds = true
                card10.scaleType = ImageView.ScaleType.FIT_CENTER
                card10.setPadding(20.toPx(), 20.toPx(), 20.toPx(), 20.toPx())
                constraintLayout.addView(card10)
                setImageButton(card10, 100.toPx())
                card10.setBackgroundResource(R.drawable.border)
                cardsArray = appendImageView(cardsArray, card10)

                set.clone(constraintLayout)
                set.connect(card10.id, ConstraintSet.TOP, guideline7.id, ConstraintSet.TOP)
                set.connect(card10.id, ConstraintSet.BOTTOM, guideline7.id, ConstraintSet.BOTTOM)
                set.connect(card10.id, ConstraintSet.START, guideline6.id, ConstraintSet.START)
                set.connect(card10.id, ConstraintSet.END, guideline6.id, ConstraintSet.END)
                set.applyTo(constraintLayout)

                randomNumbers = randomNumbersPrimitive + 4 + 4 + 5 + 5

            }
        }

        // ----------------------------  GAME -------------------------------

        shuffle(randomNumbers)

        lateinit var penultimateCard: ImageButton
        var penultimateNumber = 0
        var savedNumber = 0
        var clicks = 0
        var win = 0

        for(i in cardsArray.indices){
            cardsArray[i].setOnClickListener {
                if (!freeze) {
                    clicks++
                    if ((clicks%2)!=0) {
                        penultimateCard = cardsArray[i]
                        penultimateNumber = randomNumbers[i]
                    }
                    changeButtonImage(randomNumbers[i], cardsArray[i])
                    if (randomNumbers[i] == savedNumber || (clicks%2)!=0 ) {
                        if (randomNumbers[i] == penultimateNumber && (clicks%2)==0) {
                            win += 1
                            if (win==(cardsArray.size/2)) {
                                scoreBuilder(chronometer.base, clicks, numCards, difficulty)
                            }
                        }
                        savedNumber = randomNumbers[i]
                    } else {
                        freeze = true
                        handler.postDelayed({imageButtonReset(cardsArray[i], penultimateCard); freeze = false}, 250)
                    }
                }
            }
        }

    }

    private fun imageButtonReset(card2c1: ImageButton, card2c2: ImageButton){
        changeButtonImage(0, card2c1)
        changeButtonImage(0, card2c2)
    }

    private fun scoreBuilder(time: Long, clicks: Int, numCards: Int?, difficulty: Int) {
        var score = 999999+difficulty
        var loop = 0
        while (loop<=clicks) {
            score -= 20000
            loop++
        }
        loop = 0
        val timeOnSeconds: Long =(time/10000)
        while (loop<=timeOnSeconds) {
            score -= 25
            loop++
        }
        val intent = Intent(this, PuntuacioJoc::class.java)
        intent.putExtra("score", score)
        intent.putExtra("numCards", numCards)
        startActivity(intent)
    }

    private fun changeButtonImage(numImage: Int, fish: ImageButton) {
        val images: Array<Int> = arrayOf(0, R.drawable.pescado1, R.drawable.pescado2, R.drawable.pescado3, R.drawable.pescado4, R.drawable.pescado5)
        fish.setImageResource(images[numImage])
    }

//    private fun changeButtonImage(numImage: Int, fish: ImageButton) {
//        var viewModel = ViewModelProvider(this).get(JocViewModel::class.java)
//        fish.setImageResource(viewModel.changeButtonImage(numImage))
//    }

    private fun setGuideline(guideline: Guideline, percent: Float){
        val params = guideline.layoutParams as ConstraintLayout.LayoutParams
        params.guidePercent = percent
        guideline.layoutParams = params
    }

    private fun setImageButton(imageButton: ImageButton, wandh: Int){
        val params = imageButton.layoutParams as ConstraintLayout.LayoutParams
        params.width = wandh
        params.height = wandh
    }

    private fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()

    private fun appendImageView(arr: Array<ImageButton>, element: ImageButton): Array<ImageButton> {
        val list: MutableList<ImageButton> = arr.toMutableList()
        list.add(element)
        return list.toTypedArray()
    }

}